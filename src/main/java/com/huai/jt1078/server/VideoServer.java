package com.huai.jt1078.server;

import com.huai.jt1078.config.NettyConfig;
import com.huai.jt1078.handler.LengthDecoder;
import com.huai.jt1078.handler.MessageDecoderHandler;
import com.huai.jt1078.handler.MessageHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author xingkong
 * @program jt1078
 * @description 视频服务器
 * @date 2021-09-04 10:16
 **/
@Slf4j
@Service
public class VideoServer {

    private final LoggingHandler loggingHandler;
    private final MessageDecoderHandler messageDecoderHandler;
    private final MessageHandler messageHandler;
    private final NettyConfig.NettyServerConfig serverConfig;

    public VideoServer(MessageDecoderHandler messageDecoderHandler, MessageHandler messageHandler, NettyConfig.NettyServerConfig serverConfig) {
        this.messageHandler = messageHandler;
        this.loggingHandler = new LoggingHandler(LogLevel.DEBUG);
        this.messageDecoderHandler = messageDecoderHandler;
        this.serverConfig = serverConfig;
    }

    public void start() {
        try {

            NioEventLoopGroup boss = new NioEventLoopGroup();
            NioEventLoopGroup worker = new NioEventLoopGroup();
            // 服务器
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            // 通道
            serverBootstrap.channel(NioServerSocketChannel.class);
            // 工作组
            serverBootstrap.group(boss, worker);

            serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {

                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    ChannelPipeline pipeline = socketChannel.pipeline();
                    // TODO 这里添加日志处理器 正式使用时需要注释这一行代码
                    pipeline.addLast(loggingHandler);
                    // 添加心跳响应 10秒响应一次心跳
                    pipeline.addLast(new IdleStateHandler(10, 0, 0, TimeUnit.SECONDS));
                    // 报文定长解析器
                    pipeline.addLast(new LengthDecoder());
                    // 解析报文转换为数据实体
                    pipeline.addLast(messageDecoderHandler);
                    // 数据处理
                    pipeline.addLast(messageHandler);
                }
            });

            ChannelFuture channelFuture = serverBootstrap.bind(serverConfig.getTcpPort()).sync();
            log.info("启动netty,接受车载视频端口 port:{}", serverConfig.getTcpPort());

            // 优雅的关闭服务器
            Channel channel = channelFuture.channel();
            channel.closeFuture().addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture channelFuture) throws Exception {
                    boss.shutdownGracefully();
                    worker.shutdownGracefully();
                }
            });


        } catch (Exception e) {
            log.error("---------------netty服务器启动失败---------------", e);
        }

    }
}
