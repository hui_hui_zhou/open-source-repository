package com.huai.jt1078.handler.video;

import com.huai.jt1078.entity.VisualAudio;
import com.huai.jt1078.enums.VisualAudioType;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.stereotype.Service;

/**
 * @author xingkong
 * @program jt1078
 * @description 音频处理器
 * @date 2021-09-06 11:24
 **/
@Service
public class AudioHandler extends VisualAudioHandler{

    @Override
    protected VisualAudioType getClassName() {
        return VisualAudioType.AUDIO;
    }

    @Override
    public void handler(VisualAudio visualAudio, ChannelHandlerContext ctx) {
        super.init(visualAudio,ctx);
        // 推送视频流
        videoUtil.publishAudio(visualAudio);
    }
}
