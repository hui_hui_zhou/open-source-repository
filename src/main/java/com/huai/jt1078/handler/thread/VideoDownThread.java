//package com.huai.jtt1078.handler.thread;
//
//import org.bytedeco.javacv.FFmpegFrameGrabber;
//import org.bytedeco.javacv.FFmpegFrameRecorder;
//import org.bytedeco.javacv.Frame;
//
///**
// * @author xingkong
// * @program jt1078-video-server
// * @description
// * @date 2021-09-13 11:22
// **/
//public class VideoDownThread extends Thread{
//
//    private final String tag;
//
//    public VideoDownThread(String tag) {
//        this.tag = tag;
//    }
//
//    private void recordByFrame(FFmpegFrameGrabber grabber, FFmpegFrameRecorder recorder, Long m)
//            throws Exception{
//        try {
//            grabber.start();
//            recorder.setVideoCodec(grabber.getVideoCodec());
//            recorder.start();
//            Frame frame = null;
//            //计算帧数
//            System.out.println("1----->");
//            System.out.println("2----->" + grabber.grabFrame());
//            // 已经录制帧数
//            while ( (frame = grabber.grabFrame()) != null) {
//                recorder.record(frame);
//            }
//            recorder.stop();
//            grabber.stop();
//        } finally {
//            if (grabber != null) {
//                grabber.stop();
//            }
//        }
//    }
//
//    @Override
//    public void run() {
//        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber("rtmp://10.50.100.90/live/"+tag);
//        //帧数
//        grabber.setFrameRate(15);
//        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder("D://"+tag+".mp4", 352, 288,2);
//        recorder.setFrameRate(15);
//        recorder.setVideoBitrate(2000000);
//        try {
//            // 视频是15帧的
//            this.recordByFrame(grabber, recorder, 15L);
//        }catch (Exception e){
//            e.printStackTrace();
//            System.out.println("下载异常");
//        }
//    }
//
//
//
//
//}
