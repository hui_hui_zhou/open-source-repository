package com.huai.jt1078;

import com.huai.jt1078.server.HttpServer;
import com.huai.jt1078.server.VideoServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

/**
 * @author xingkong
 * @date 2021年9月4日10:11:37
 */
@Slf4j
@SpringBootApplication
public class Jtt1078Application {

    public static void main(String[] args) {
        ApplicationContext context = new SpringApplicationBuilder(Jtt1078Application.class)
                .web(WebApplicationType.SERVLET)
                .run(args);
        // 启动netty服务器
        VideoServer videoServer = context.getBean(VideoServer.class);
        videoServer.start();
        HttpServer httpsServer = context.getBean(HttpServer.class);
        try {
            httpsServer.start();
        }catch (Exception e){
           log.error("启动http server 失败",e);
        }
    }

}
