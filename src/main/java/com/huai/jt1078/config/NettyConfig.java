package com.huai.jt1078.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @program: chatServer
 * @description: netty配置
 * @author: huai
 * @create: 2021-05-12 21:49
 **/
public class NettyConfig {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Component
    @ConfigurationProperties(prefix = "netty.server")
    public static class NettyServerConfig {
        /**
         * netty server port
         */
        private Integer tcpPort;

        /**
         * 主线程最大线程数
         */
        private Integer bossMaxThreadCount = 1;

        /**
         * 工作线程最大线程数
         */
        private Integer workMaxThreadCount = 8;

        /**
         * 数据包最大长度
         */
        private Integer maxFrameLength = 65535;

        /**
         * 单节点最大连接数
         */
        private Integer maxConnectNum;

        /**
         * 读事件空闲时间 秒
         */
        private Integer readerIdleTimeSeconds = 180;

        /**
         * 写事件空闲时间 秒
         */
        private Integer writerIdleTimeSeconds = 180;
        /**
         * 读写都空闲事件 秒
         */
        private Integer allIdleTimeSeconds = 180;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Component
    @ConfigurationProperties(prefix = "netty.client")
    public static class NettyClientConfig {
        /**
         * netty client ip
         */
        private String tcpIp;
        /**
         * netty client port
         */
        private Integer tcpPort;
        /**
         * 读事件空闲时间 秒
         */
        private Integer readerIdleTimeSeconds = 60;

        /**
         * 写事件空闲时间 秒
         */
        private Integer writerIdleTimeSeconds = 60;
        /**
         * 读写都空闲事件 秒
         */
        private Integer allIdleTimeSeconds = 60;
    }
}
