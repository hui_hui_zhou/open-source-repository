package com.huai.jt1078.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author xingkong
 * @program jt1078
 * @description 视频配置
 * @date 2022-01-04 17:49
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "video")
public class VideoConfig {


    /** 公网端口  */
    private String serverPort;
    /** 自己的http接口 默认是3333  */
    private int httpPort;
    /** 公网ip  */
    private String serverIp;

    /** flv视频播放地址  */
    private String flvUrl;
    /** rtmp播放地址  */
    private String rtmpUrl;
    /** hls视频播放地址  */
    private String hlsUrl;
    /** websocket地址  */
    private String wsUrl;
    /**  是否开启调试模式 - 会在后台打印流信息 */
    private boolean debugMode;
    /** 是够保存历史视频  */
    private boolean historySave;
    /** 历史视频保存地址 - 如果开启了历史视频保存的话  */
    private String historyPath;
    /** 是否开启心跳  */
    private boolean heartbeat;
    /** 播放转出流**/
    private String mode;

//    FLV("flv","flv.url"),
//    RTMP("rtmp","rtmp.url"),
//    HLS("hls","hls.url"),
//    AUDIO("audio","flv.url");
    public String getUrl(String type){
        switch (type){
            case "flv":
                return  flvUrl;
            case "rtmp":
                return  rtmpUrl;
            case "hls":
                return  hlsUrl;
            default:
                return  flvUrl;
        }
    }

}
