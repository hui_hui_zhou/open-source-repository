package com.huai.jt1078.enums;

import lombok.Getter;

/**
 * @author xingkong
 * @program jt1078-video-server
 * @description: 视频格式
 * @create 2021-09-02 15:20
 **/
public enum VideoType {



    // 视频地址
    FLV("flv","flv.url"),
    RTMP("rtmp","rtmp.url"),
    HLS("hls","hls.url"),
    AUDIO("audio","flv.url");

    @Getter
    private final String type;

    @Getter
    private final String url;

    VideoType(String type, String url) {
        this.type = type;
        this.url = url;
    }

    /**
     * 根据视频类型返回配置信息
     * @param type 视频类型
     * @author xingkong
     * @date 2021-09-02 15:38
     * @return 视频对应的配置key
     */
    public static String getUrlKey(String type){
        for (VideoType videoType : values()){
            if (videoType.type.equals(type)){
                return videoType.url;
            }
        }
        return null;
    }
}
