package com.huai.jt1078.enums;

import lombok.Getter;

/**
 * @author xingkong
 * @program jt1078
 * @description 音视频类型
 * @date 2021-09-06 10:58
 **/
public enum VisualAudioType {

    // 音频和视频类型
    VIDEO("videoHandler","视频处理器"),
    AUDIO("audioHandler","音频处理器"),
    INTERCOM("intercom","对讲处理器");


    @Getter
    private final String className;

    @Getter
    private final String label;


    VisualAudioType(String className, String label) {
        this.className = className;
        this.label = label;
    }
}
