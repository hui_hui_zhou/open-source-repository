package com.huai.jt1078.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

/**
 * @author xingkong
 * @program jt1078
 * @description 数据类型
 * @date 2021-09-04 13:31
 **/
public enum DataType {

    // 数据类型
    VIDEO_I(0, "视频I帧"),
    VIDEO_P(1, "视频P帧"),
    VIDEO_B(2, "视频B帧"),
    AUDIO(3, "音频"),
    PASS(4, "透传");

    @Getter
    private final Integer type;

    @Getter
    private final String label;

    DataType(Integer type, String label) {
        this.type = type;
        this.label = label;
    }

    /**
     * 获取视频的类型
     *
     * @return
     */
    public static List<Integer> getVideoTypes() {
        return Arrays.asList(VIDEO_I.type, VIDEO_P.type, VIDEO_B.type);
    }
}
