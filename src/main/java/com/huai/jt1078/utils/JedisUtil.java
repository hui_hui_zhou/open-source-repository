package com.huai.jt1078.utils;


import java.util.concurrent.TimeUnit;

/**
 * @author xingkong
 * @program jt1078-video-server
 * @description jedis工具类
 * @date 2021-09-08 14:16
 **/
public class JedisUtil {





    private static int defaultTimeout = -1;

    /**
     * 设置值
     * @param key 主键
     * @param value  值
     * @param timeout  超时时间（秒）
     * @author xingkong
     * @date 2021-09-08 14:23
     */
    public static void set(String key,String value,int timeout){
        SessionManager.instance.getRedisTemplate().opsForValue().set("local:" + key,value);
        // 如果有设置超时时间 则设置超时时间
        if (defaultTimeout != timeout){
            SessionManager.instance.getRedisTemplate().expire("local:" + key,timeout, TimeUnit.SECONDS);
        }
    }

    /**
     * 判断某个key是否还存在
     * @param key 检查的key
     * @author xingkong
     * @date 2021-09-08 14:27
     * @return 是否存在
     */
    public static Boolean exists(String key){
        Boolean aBoolean = SessionManager.instance.getRedisTemplate().hasKey("local:" + key);
        return aBoolean;
    }
}
