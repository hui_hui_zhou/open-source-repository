package com.huai.jt1078.utils;

import com.huai.jt1078.codec.AudioCodec;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;

/**
 * @author xingkong
 * @program jt1078
 * @description websocket工具类
 * @date 2021-09-22 17:02
 **/
public class WebsocketUtil {
    @Getter
    private final ChannelHandlerContext context;
    @Getter
    private final AudioCodec audioCodec;

    private int sequence;
    private long startTime = -1;


    public WebsocketUtil(String tag){
        context = SessionManager.instance.getTagChannelMap().get(tag);
        VideoUtil videoUtil = SessionManager.instance.getVideoUtils().get(tag);
        audioCodec = videoUtil.getAudioCodec();
        sequence = 0;
        startTime = System.currentTimeMillis();
    }

    public int getSequence(){
        return ++sequence;
    }

    public int getTime(){
       if (startTime == -1){
           startTime = System.currentTimeMillis();
           return 0;
       }
       return (int)(System.currentTimeMillis() - startTime);
    }

}
