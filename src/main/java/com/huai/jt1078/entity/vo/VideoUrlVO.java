package com.huai.jt1078.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author xingkong
 * @program jt1078-video-server
 * @description 视频地址请求返回实体
 * @create 2021-09-02 15:58
 **/
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class VideoUrlVO {

    /** 视频请求地址 */
    private String videoUrl;

    /** 本地服务端口 */
    private String serverPort;

    /** 本地服务ip */
    private String serverIp;

    /** websocket地址 */
    private String webSocketUrl;

}
