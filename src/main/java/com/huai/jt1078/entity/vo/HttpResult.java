package com.huai.jt1078.entity.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhoujiwei
 * @create 2018-07-23 10:24
 * @desc
 **/
public enum HttpResult {

    /**
     * 请求成功返回码.
     */
    SUCCESS("请求成功", 0),

    /**
     * 请求失败返回描述及返回码.
     */
    FAIL("请求失败", -1),

    /**
     * 不合法的参数返回描述及返回码.
     */
    PARAMETER_ERROR("不合法的参数", 40035),

    /**
     * 系统异常返回描述及返回码.
     */
    SYSTEM_ERROR("系统错误", 61450);

    @Setter
    @Getter
    private int code;

    @Setter
    @Getter
    private String message;

    HttpResult(String message, int code) {
        this.code = code;
        this.message = message;
    }
}
