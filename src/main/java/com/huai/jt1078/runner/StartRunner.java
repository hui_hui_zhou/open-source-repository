package com.huai.jt1078.runner;

import com.huai.jt1078.utils.SessionManager;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @author xingkong
 * @program paas-common-video-service
 * @description 启动类型
 * @date 2022-01-04 15:24
 **/
@Component
public class StartRunner implements ApplicationRunner {

    private final RedisTemplate<String, String> redisTemplate;

    public StartRunner(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        SessionManager.instance.setRedisTemplate(redisTemplate);
    }
}
