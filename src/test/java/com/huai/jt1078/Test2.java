package com.huai.jt1078;

import com.huai.jt1078.entity.SpsFrame;
import com.huai.jt1078.utils.H264SPSPaser;

/**
 * @author xingkong
 * @program jt1078
 * @description
 * @date 2021-09-13 14:35
 **/
public class Test2 {

    public static void main(String[] args) {
        byte[] sps2 = {0x67, 0x42, 0x00, 0x0a, (byte) 0xf8, 0x41, (byte) 0xa2};
        SpsFrame spsFrame = new SpsFrame().getSpsFrame(sps2);
        H264SPSPaser h264SPSPaser = new H264SPSPaser();
        System.out.print(h264SPSPaser.getWidth(spsFrame)+ "\n");
        System.out.print(h264SPSPaser.getHeight(spsFrame)+ "\n");

    }

}
