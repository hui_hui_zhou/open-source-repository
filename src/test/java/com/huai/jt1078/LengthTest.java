package com.huai.jt1078;

/**
 * @author xingkong
 * @program jt1078
 * @description
 * @date 2021-09-04 10:44
 **/
public class LengthTest {

    public static void main(String[] args) {

        int length = 1025;
        for (int i = 0, k = (int) Math.ceil(length / 512f); i < k; i++) {
            int l = i < k - 1 ? 512 : length - (i * 512);

            System.out.println(l);
        }

    }
}
