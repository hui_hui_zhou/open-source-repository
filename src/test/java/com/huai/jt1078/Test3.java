package com.huai.jt1078;

import cn.hutool.core.text.StrSpliter;

import java.util.Arrays;

/**
 * @author xingkong
 * @program jt1078
 * @description
 * @date 2021-09-16 10:45
 **/
public class Test3 {


    public static void main(String[] args) {
        String tag = "013845687541";
        String[] strings = StrSpliter.splitByLength(tag, 2);
        System.out.println(Arrays.toString(strings));
    }
}
