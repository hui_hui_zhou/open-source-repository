package com.huai.jt1078;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

/**
 * @author xingkong
 * @program jt1078
 * @description
 * @date 2021-09-04 11:04
 **/
public class ByteTest {

    public static void main(String[] args) {
        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer(8);

        int a = 130;

        buffer.writeByte((byte)(a>>24 & 0xff));
        buffer.writeByte((byte)(a>>16 & 0xff));
        buffer.writeByte((byte)(a>>8 & 0xff));
        buffer.writeByte((byte)(a & 0xff));

        buffer.writeInt(a);

        System.out.println(buffer.readInt());

        System.out.println(buffer.readInt());
    }
}
