package com.huai.jt1078;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @author xingkong
 * @program jt1078
 * @description 视频推送测试类
 * @date 2021-09-04 12:44
 **/
public class    VideoPushTest {

    public static void main(String[] args) throws Exception {
        Socket conn = new Socket("localhost", 1078);
        OutputStream os = conn.getOutputStream();

        // InputStream fis = new FileInputStream("e:\\workspace\\enc_dec_audio\\streamax.bin");
        // InputStream fis = new FileInputStream("e:\\test\\streaming.hex");
        InputStream fis = VideoPushTest.class.getResourceAsStream("/test01.bin");
        int len = -1;
        byte[] block = new byte[1024];
        while ((len = fis.read(block)) > -1)
        {
            os.write(block, 0, len);
            os.flush();
            Thread.sleep(20);
            System.out.println("sending...");
        }
        os.close();
        fis.close();
        conn.close();
    }
}
