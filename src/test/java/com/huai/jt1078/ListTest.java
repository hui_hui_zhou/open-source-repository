package com.huai.jt1078;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author xingkong
 * @program jt1078
 * @description
 * @date 2022-01-04 14:24
 **/
public class ListTest {

    public static void main(String[] args) {
        List<byte[]> list = new ArrayList<>(10);


        list.add(new byte[]{1,3});
        list.add(new byte[]{7,8});
        list.add(new byte[]{3,8});
        list.add(new byte[]{5,8});
        list.add(new byte[]{6,8});

        byte[] pcmData = new byte[10];
        int dataIndex = 0;
        for (byte[] tempData : list) {
            System.arraycopy(tempData, 0, pcmData, dataIndex, tempData.length);
            dataIndex += tempData.length;
        }

        System.out.println(Arrays.toString(pcmData));
    }
}
